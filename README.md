# e03

## Vert.x

- Go to https://start.vertx.io/
- Generate the starter kit
- Upload it
- unzip
- remove `.mvn` directory

## 2 runners

### Maven

```bash
gitlab-runner register --non-interactive \
  --url ${gitlab_url} \
  --name ${runner_name} \
  --registration-token ${gitlab_token} \
  --executor docker \
  --tag-list "maven" \ # 🖐️
  --docker-image maven:latest # 🖐️
```

### Code Quality

```bash
gitlab-runner register --non-interactive \
  --url ${gitlab_url} \
  --name ${runner_name} \
  --registration-token ${gitlab_token} \
  --executor docker \
  --tag-list "code-quality" \ # 🖐️
  --locked="false" \
  --access-level="not_protected" \
  --docker-volumes "/cache"\
  --docker-volumes "/tmp/builds:/tmp/builds"\
  --builds-dir "/tmp/builds" \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \\
  --docker-image docker:stable # 🖐️
```

Ref: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#set-up-a-private-runner-for-code-quality-without-docker-in-docker

**Remark**: it's possible to use your own tools with the code quality feature
